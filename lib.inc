
section .text
 
; Принимает код возврата и завершает текущий процесс
; из семинара
exit: 
    mov  rax, 60            ; invoke 'exit' system call
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; из семинара
string_length:
    xor rax, rax
.counter:
    cmp  byte [rdi+rax], 0
    je   .end
    inc  rax
    jmp  .counter
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    push rsi 
    call string_length
    mov rdx, rax
    pop rsi
    mov rdi, 1 
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push di
    mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop di
    ret

; Переводит строку (выводит символ с кодом 0xA)
;из семинара
print_newline:
    mov di, `\n`
    jmp print_char
	
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, rsp 
    sub rsp, 20  
    mov rax, rdi 
    mov rdi, 10
.loop:
    xor rdx, rdx 
    div rdi 
    add rdx, '0' 
    dec rcx 
    mov [rcx], dl  
    test rax, rax  
    jne .loop 
    mov rsi, rcx 
    lea rdx, [rsp + 20]
    sub rdx, rcx
    mov rax, 1
    mov rdi, 1
    syscall
    add rsp, 20 ; free stack
    ret
	
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge print_uint
.negative:
    push rdi
    mov rdi, '-'   
    call print_char 
    pop rdi
    neg rdi 
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov dl, [rdi + rcx]
    cmp byte[rsi + rcx], dl
    jne .end
    inc rcx
    test dl, dl
    jne .loop
    inc rax
.end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    sub rsp, 1
    xor rax, rax
    mov rdx, 1
    xor rdi, rdi
    mov rsi, rsp
    syscall
    test rax, rax
    jz .error
    mov al, byte [rsp]
    add rsp, 1
    ret
.error:
    add rsp, 1
    xor rax, rax
    ret
 
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    dec rsi
    push r13
    mov r13, rdi
    push r14
    mov r14, rsi
    push r15
    test r15, r15
.read_space_loop:
    call read_char
    cmp r15, r14
    jae .err
    cmp al, ` `
    je .read_space_loop
    cmp al, `\t` 
    je .read_space_loop
    cmp al, `\n`  
    je .read_space_loop
    test rax, rax
    jz .skip_symbols
    mov [r13 + r15], al
    inc r15

.read_char_loop:
    call read_char
    cmp r15, r14
    jae .err
    cmp al, ` `
    je .skip_symbols
    cmp al, `\t`
    je .skip_symbols
    cmp al, `\n`
    je .skip_symbols
    cmp al , `\r`
    je .skip_symbols
    test rax, rax
    jz .skip_symbols
    mov [r13 + r15], al
    inc r15
    jmp .read_char_loop
.err:
     xor rax, rax
     xor rdx, rdx
     jmp .end
.skip_symbols:
     mov byte [r13 + r15], 0
     mov rax, r13
     mov rdx, r15
.end:
    pop r15
    pop r14
    pop r13
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
.read_loop:
    movzx rcx, byte [rdi+rdx]
    test rcx, rcx
    jz .exit
    cmp rcx, '0'
    jb .exit
    cmp rcx, '9'
    ja .exit
    sub rcx, '0'
    imul rax, rax, 10
    add  rax, rcx
    inc  rdx
    jmp  .read_loop
.exit:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov rcx, rdi 
    cmp byte[rcx], '-'
    jne .positive
.negative:
    inc rcx ;
    mov rdi, rcx
    push rcx
    call parse_uint
    pop rcx
    neg rax 
    inc rdx
    ret
.positive:
    mov rdi, rcx
    jmp parse_uint

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdi
    push rdx
    push rsi
    call string_length
    pop rsi
    pop rdx
    pop rdi
    cmp rax, rdx
    jae .error
    xor r8, r8
.loop:
    xor r9, r9
    mov r9b, byte [rdi + r8]
    mov byte[rsi + r8], r9b
    test r9b, r9b
    je .end
    inc r8
    jmp .loop
.error:
     xor rax, rax
.end:
    ret